#include"Multiple.h"

void multiple(void)
{
	int i;
	for (i = 0; i < 20; ++i) {
		if (i % 2 == 0 || i % 3 == 0)
			continue;
		printf("%d\n", i);
	}
}
