#include<stdio.h>
#include<string.h>
#define MAX 50

typedef struct information // 전화번호부에 저장될 요소
{
	char name[20];
	char phone[20];
} REG;

void search_all(REG * reg, int * person)
{
	int i;

	printf("\n  << 전화번호 목록 >>  \n");
	for (i = 0; i < (*person); i++)
		printf("%s      \t%s\n", reg[i].name, reg[i].phone);
	//   printf("\n============================================\n\n");
}

int search_person(REG * reg, char * name, int * person)
{
	int i;

	for (i = 0; i < (*person); i++)
	{
		if (!strcmp(reg[i].name, name))
		{
			printf("%s      \t%s\n\n", reg[i].name, reg[i].phone);
			//      printf("============================================\n\n");
			return 0;
		}
		else
			continue;
	}
	printf("\n일치하는 이름이 없습니다!\n\n");
	//   printf("============================================\n\n");
	return 0;
}
