#pragma once
#include<stdio.h>
#include<string.h>
#define MAX 50

typedef struct information // 전화번호부에 저장될 요소
{
	char name[20];
	char phone[20];
} REG;


void search_all(REG * reg, int * person);
int search_person(REG * reg, char * name, int * person);
int admit(REG * reg, int * person);
int del(REG * reg, int * person);
