#include<stdio.h>
#include <string.h>
#define MAX 50

typedef struct information // 전화번호부에 저장될 요소
{
	char name[20];
	char phone[20];
} REG;


int del(REG * reg, int * person)
{
	if ((*person) > 0)
	{
		printf("\n삭제할 이름: ");
		scanf("%s", reg[*person].name);
		printf("전화번호: ");
		scanf("%s", reg[*person].phone);

		printf("\n%s 정보 삭제 완료!\n\n", reg[*person].name);
		//   printf("============================================\n\n");
		(*person)--;
		return 0;
	}
	else
	{
		printf("\n전화번호부가 비어 있습니다!\n\n");
		//      printf("============================================\n\n");
	}
	return 0;
}
