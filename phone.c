#include <stdio.h>
#include<string.h>
#include "rsd.h"
#define MAX 50

int trial = 0; // 비밀번호 시도횟수
char PW[50] = "qwer1234"; // 비밀번호

int is_PassWord(char * password) // 비밀번호 검증 함수; 참이면 return 1; 거짓이면 return 0;
{
	while (trial < 3)
	{
		if (!strcmp(password, PW)) // strcmp함수는 참이면 0을 return 함
		{
			trial = 0; // 비밀번호 오류 횟수 초기화
			return 1;
		}
		else
		{
			trial++;
			printf("비밀번호(%d회오류): ", trial);
			scanf("%s", password);
		}
	}
	printf("비밀번호(3회오류): 등록할 수 없습니다!\n\n");
	trial = 0; //// 비밀번호 오류 횟수 초기화
	return 0;
}

void print_menu(void)
{
	printf(" 전화번호 관리 \n\n");
	printf("1. 등록[Tab] 2. 전체검색[Tab] 3. 특정인검색[Tab] 4. 삭제 5. 종료\n\n");
	printf("메뉴 선택: ");
}

int main(void)
{
	int opt; // 사용자가 선택한 메뉴
	int person = 0; // 전화번호부에 저장된 사람 수
	char password[50]; // 사용자가 입력한 비밀번호가 저장되는 배열
	char search[20]; // 사용자가 검색할 사람의 이름
	REG reg[MAX]; // 사용자가 입력한 특정인의 정보가 저장될 구조체 배열

	while (1)
	{
		print_menu(); // 메뉴      
		scanf("%d", &opt); // 메뉴 선택

		switch (opt)
		{
		case 1: // 1. 등록
		{
			printf("비밀번호: ");
			scanf("%s", password);

			if (is_PassWord(password))
				admit(reg, &person);
			else
			{
				printf("프로그램을 종료합니다!\n\n");
				//         printf("============================================\n\n");
				return 0;
			}
			break;
		}
		case 2: // 2. 전체검색
		{
			search_all(reg, &person);
			break;
		}
		case 3: // 3. 특정한검색
		{
			printf("검색할 이름: ");
			scanf("%s", search);
			search_person(reg, search, &person);
			break;
		}
		case 4: // 4. 삭제
		{
			printf("비밀번호: ");
			scanf("%s", password);

			if (is_PassWord(password))
				del(reg, &person);
			else
			{
				printf("프로그램을 종료합니다!\n\n");
				//         printf("============================================\n\n");
				return 0;
			}
			break;
		case 5: // 5. 종료
		{
			printf("\n프로그램을 종료합니다!\n\n");
			//      printf("============================================\n\n");
			return 0;
		}
		}
		default: // continue;
		{
			printf("선택오류! 초기 화면으로 돌아갑니다.\n");
			continue;
		}
		}
	}
	return 0;
}
